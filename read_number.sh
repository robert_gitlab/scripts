#!/bin/bash
#
# Example script use standard input (positive integers only)
#

read -e -p "Give a number:" NUMBER

if ! [[ "$NUMBER" =~ ^[0-9]+$ ]];
then
  echo "Please provide number:"
else
  echo "The number you provided are:  $NUMBER"
fi
